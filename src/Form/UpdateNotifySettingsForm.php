<?php

namespace Drupal\update_notify\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\update_notify\NotifyService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Settings form for update_notify.
 */
class UpdateNotifySettingsForm extends ConfigFormBase {

  /**
   * Config key used in config.
   */
  const CONFIG_KEY = 'update_notify.settings';

  /**
   * Notify service.
   *
   * @var \Drupal\update_notify\NotifyService
   */
  protected NotifyService $notify;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Config\ImmutableConfig definition.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs the object.
   *
   * @param \Drupal\update_notify\NotifyService $notify
   *   Notify service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The RequestStack service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The RequestStack service.
   */
  public function __construct(
    NotifyService $notify,
    MessengerInterface $messenger,
    ModuleHandlerInterface $module_handler,
    RequestStack $request_stack,
    ConfigFactoryInterface $config_factory
  ) {
    $this->notify = $notify;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->requestStack = $request_stack;
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('slack.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('update_notify.notify'),
      $container->get('messenger'),
      $container->get('module_handler'),
      $container->get('request_stack'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'update_notify_settings_form';
  }

  /**
   * Build the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG_KEY);

    $form['notification_frequency'] = [
      '#markup' => '<hr /><h3>' . $this->t('Notifications Frequency') . '</h3>',
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('When enabled the checking for updates will be done when the cron runs.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['frequency'] = [
      '#type' => 'select',
      '#title' => $this->t('Frequency'),
      '#description' => $this->t('How often should checks/notify be sent?'),
      '#options' => [
        'daily' => $this->t('Once a day'),
        'weekly' => $this->t('Once a week'),
        'monthly' => $this->t('Once a month'),
      ],
      '#default_value' => $config->get('frequency'),
      '#states' => [
        'visible' => [
          ':checkbox[name="enabled"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $last_sent = $this->notify->getLastSent(TRUE);
    $next_notification = $this->notify->getNextSend(TRUE);
    $markup = '<p>' . '<strong>' . $this->t('Last notification was sent:') . '</strong> ' . $last_sent . '</p>';
    $markup .= '<p>' . '<strong>' . $this->t('Next notification:') . '</strong> ' . $next_notification . '</p>';
    $form['message'] = [
      '#markup' => $markup,
    ];

    $form['notification_settings'] = [
      '#markup' => '<hr /><h3>' . $this->t('Notification Settings') . '</h3>',
    ];

    $form['include_key'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include key'),
      '#description' => $this->t('Message to include a key explaining some of the emojis/icons.'),
      '#default_value' => $config->get('include_key'),
    ];

    $form['include_php'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include PHP version'),
      '#description' => $this->t('Message to include the current PHP version.'),
      '#default_value' => $config->get('include_php'),
    ];

    // $form['include_drush'] = [
    // '#type' => 'checkbox',
    // '#title' => $this->t('Include Drush version'),
    // '#description' =>
    // $this->t('Message to include the current Drush version.'),
    // '#default_value' => $config->get('include_drush'),
    // ];
    // $form['include_node'] = [
    // '#type' => 'checkbox',
    // '#title' => $this->t('Include Node version'),
    // '#description' =>
    // $this->t('Message to include the current Node version.'),
    // '#default_value' => $config->get('include_node'),
    // ];
    $form['include_host'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include host name'),
      '#description' => $this->t('Adds the site url to help with identification.'),
      '#default_value' => $config->get('include_host'),
    ];

    $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $form['custom_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom host value'),
      '#description' => $this->t('Add a custom value to be sent with notifications. Leave empty for %current (current host)', ['%current' => $host]),
      '#default_value' => $config->get('custom_host'),
      '#attributes' => [
        'placeholder' => $host,
      ],
      '#states' => [
        'visible' => [
          ':checkbox[name="include_host"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['notification_methods'] = [
      '#markup' => '<hr /><h3>' . $this->t('Notification Methods') . '</h3>',
    ];

    $email = $this->config('system.site')->get('mail');
    $form['email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Email notification'),
      '#description' => $this->t('Send the notification via email.'),
      '#default_value' => $config->get('email'),
    ];
    $form['email_to'] = [
      '#type' => 'email',
      '#title' => $this->t('Send email to this address'),
      '#description' => $this->t('Send the email to the specified address. Leave empty for %email (site administrator)',
        ['%email' => $email]),
      '#default_value' => $config->get('email_to'),
      '#attributes' => [
        'placeholder' => $email,
      ],
      '#states' => [
        'visible' => [
          ':checkbox[name="email"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $slack_enabled = $this->moduleHandler->moduleExists('slack');
    $is_slack_configured = $this->config->get('slack_webhook_url') ?: NULL;
    $slack_description = $this->t('Send the notification via slack.');
    $options = [
      'attributes' => [
        'target' => '_blank',
      ],
    ];
    if ($slack_enabled && !$is_slack_configured) {
      $url = Url::fromRoute('slack.admin_settings');
      $url->setOptions($options);
      $slack_description = '<p>' . $this->t('NOTICE: While Slack module is enabled, it is not properly configured. Please <a href="@configure-slack">configure Slack</a>.',
          ['@configure-slack' => $url->toString()]) . '</p>';
    }
    else {
      if (!$slack_enabled) {
        $url = Url::fromUri('https://www.drupal.org/project/slack');
        $url->setOptions($options);
        $link = Link::fromTextAndUrl($this->t('Slack module'), $url)
          ->toString();
        $slack_description = '<p>' . $this->t('To use Slack notifications you need to install and configure the Slack module.') . '<br/>' . $link . '</p>';
      }
    }

    $channel = "#unknown";
    if ($slack_enabled && $is_slack_configured) {
      $channel = $this->config
      // Get the channel name from Slack settings.
        ->get('slack_channel');
    }

    $form['slack'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Slack notification'),
      '#description' => $slack_description,
      '#default_value' => $config->get('slack'),
      '#attributes' => [
        'disabled' => !$is_slack_configured,
      ],
    ];

    $form['slack_channel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Send notifications to this channel'),
      '#description' => $this->t('Send the Slack notifications to the channel specified. Leave empty for %channel (Slack default)',
        ['%channel' => $channel]),
      '#default_value' => $config->get('slack_channel'),
      '#attributes' => [
        'placeholder' => $channel,
      ],
      '#states' => [
        'visible' => [
          ':checkbox[name="slack"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['actions']['#type'] = 'actions';
    if ($config->get('enabled')) {
      $form['actions']['notify_now'] = [
        '#type' => 'submit',
        '#value' => $this->t('Notify now'),
        '#submit' => [[$this, 'notifyNow']],
      ];
    }

    $form['#theme'] = 'system_config_form';
    return parent::buildForm($form, $form_state);
  }

  /**
   * Notify now action.
   *
   * @param array $form
   *   From array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Use form_state.
   */
  public function notifyNow(array &$form, FormStateInterface $form_state) {
    $this->saveConfig($form, $form_state);
    $response = $this->notify->triggerNotifications(TRUE);
    if ($response['success']) {
      $this->messenger->addMessage($this->t('Notifications sent, with @count module updates found.', [
        '@count' => $response['count'],
      ]));
    }
    else {
      $this->messenger->addError($this->t('Failed to send notifications.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->saveConfig($form, $form_state);
  }

  /**
   * Extra save method, so we can call save in both actions.
   *
   * @param array $form
   *   Use Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Use form_state.
   */
  protected function saveConfig(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);
    $prev_frequency = $config->get('frequency');

    foreach ([
      'enabled',
      'frequency',
      'include_key',
      'include_php',
    // 'include_drush',
    // 'include_node',
      'include_host',
      'custom_host',
      'email',
      'email_to',
      'slack',
      'slack_channel',
    ] as $key) {
      $config->set($key, $form_state->getValue($key));
    }

    $config->save();

    // If the frequency has changed update the next send time.
    if ($form_state->getValue('frequency') != $prev_frequency) {
      $this->notify->updateNextSend();
    }
  }

}
