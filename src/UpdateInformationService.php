<?php

namespace Drupal\update_notify;

use Drupal\Core\Extension\ExtensionVersion;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\update\ProjectRelease;
use Drupal\update\UpdateFetcherInterface;
use Drupal\update\UpdateManagerInterface;

/**
 * Update Information service.
 */
class UpdateInformationService {
  use StringTranslationTrait;

  /**
   * Constructs a new UpdateInformationService object.
   */
  public function __construct() {

  }

  /**
   * Pulls out the update data and sorts it ready for notifications.
   *
   * @return array|null
   *   Return array
   */
  public function checkForUpdates(): ?array {
    $projects = [];

    // Get all available modules.
    $available = update_get_available(TRUE);

    // If we have nothing return null, this should be handled as an error.
    if (empty($available)) {
      return NULL;
    }

    // Get the update project data.
    $project_data = update_calculate_project_data($available);

    foreach ($project_data as $name => $project) {
      $fetch_failed = FALSE;
      $data = [];

      // Flag if we have failed to get data.
      if ($project['status'] === UpdateFetcherInterface::NOT_FETCHED) {
        $fetch_failed = TRUE;
      }

      // Filter out projects which are up-to-date already.
      if ($project['status'] == UpdateManagerInterface::CURRENT) {
        continue;
      }

      // If we don't know what to recommend they upgrade to, we should skip
      // the project entirely.
      if (empty($project['recommended'])) {
        continue;
      }

      $name = $this->findProjectName($project, $name);
      // Extract the project data.
      $data['name'] = $name['title'];
      $data['url'] = $name['link'];
      $data['installed_version'] = $project['existing_version'];
      $data['type'] = $project['project_type'];
      $data['recommended_release'] = $this->getRecommendedRelease($project);
      $data['status'] = $this->getStatusInformation($project);
      $data['fetch_failed'] = $fetch_failed;

      // If we have data about the project, add it to the projects array.
      if (!empty($data)) {
        $projects[] = $data;
      }
    }

    return $this->sortByTypes($projects);
  }

  /**
   * Sorts the projects by their types.
   *
   * @param array $projects
   *   Pass $projects.
   *
   * @return array
   *   Return $output
   */
  protected function sortByTypes(array $projects): array {
    $output = [];

    foreach ($projects as $project) {
      $output[$project['type']][] = $project;
    }

    ksort($projects);

    return $output;
  }

  /**
   * Returns the status information.
   *
   * @param array $project
   *   Pass $project.
   *
   * @return bool[]
   *   Return array
   */
  protected function getStatusInformation(array $project): array {
    $security = FALSE;
    $unsupported = FALSE;
    $recommended = FALSE;

    switch ($project['status']) {
      case UpdateManagerInterface::NOT_SECURE:
      case UpdateManagerInterface::REVOKED:
        $security = TRUE;
        break;

      case UpdateManagerInterface::NOT_SUPPORTED:
        $unsupported = TRUE;
        break;

      case UpdateFetcherInterface::UNKNOWN:
      case UpdateFetcherInterface::NOT_FETCHED:
      case UpdateFetcherInterface::NOT_CHECKED:
      case UpdateManagerInterface::NOT_CURRENT:
        $recommended = TRUE;
        break;
    }

    return [
      'security' => $security,
      'unsupported' => $unsupported,
      'recommended' => $recommended,
    ];
  }

  /**
   * Extracts recommended release data.
   *
   * @param array $project
   *   Pass $project.
   *
   * @return array
   *   Return $data
   */
  protected function getRecommendedRelease(array $project): array {
    $data = [];
    $recommended_release = ProjectRelease::createFromArray($project['releases'][$project['recommended']]);
    $recommended_version_parser = ExtensionVersion::createFromVersionString($recommended_release->getVersion());

    $data['url'] = $recommended_release->getReleaseUrl();
    $data['version'] = $recommended_release->getVersion();
    $data['date'] = $recommended_release->getDate();
    $data['is_major'] = $recommended_version_parser->getMajorVersion() != $project['existing_major'];

    return $data;
  }

  /**
   * Extract the project name from the project.
   *
   * @param array $project
   *   Pass $project.
   * @param string $name
   *   Pass $name.
   *
   * @return array
   *   Return project_name
   */
  protected function findProjectName(array $project, string $name): array {
    // The project name to display can vary based on the info we have.
    if (!empty($project['title'])) {
      if (!empty($project['link'])) {
        $project_name = [
          'title' => $project['title'],
          'link' => $project['link'],
        ];
      }
      else {
        $project_name = [
          'title' => $project['title'],
          'link' => NULL,
        ];
      }
    }
    elseif (!empty($project['info']['name'])) {
      $project_name = [
        'title' => $project['info']['name'],
        'link' => NULL,
      ];
    }
    else {
      $project_name = [
        'title' => $name,
        'link' => NULL,
      ];
    }
    if ($project['project_type'] == 'theme' || $project['project_type'] == 'theme-disabled') {
      $project_name['title'] = $project_name['title'] . ' ' . $this->t('(Theme)');
    }

    return $project_name;
  }

}
