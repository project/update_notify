<?php

namespace Drupal\update_notify;

/**
 * Creates a Markdown document based on the parsed documentation.
 *
 * @package Apidoc
 * @version 1.00 (2014-04-04)
 * @license GNU Lesser Public License
 */
class TextTable {

  /**
   * The maximum length of a cell.
   *
   * @var int
   */
  public int $maxlength = 250;

  /**
   * The data for the area [key => label, ...].
   *
   * @var array
   */
  private array $data = [];

  /**
   * The header array [key => label, ...].
   *
   * @var array
   */
  private array $header = [];

  /**
   * The length of the columns.
   *
   * @var array
   */
  private array $len = [];

  /**
   * The alignment options [key => L|R|C, ...].
   *
   * @var array
   */
  private array $align = [
    'name' => 'L',
    'type' => 'C',
  ];

  /**
   * Construct.
   *
   * @param array $header
   *   The header array [key => label, ...].
   * @param array $content
   *   Content.
   * @param array|null $align
   *   Alignment options [key => L|R|C, ...].
   */
  public function __construct(array $header = [], array $content = [], array $align = NULL) {
    if ($header) {
      $this->header = $header;
    }
    elseif ($content) {
      foreach ($content[0] as $key => $value) {
        $this->header[$key] = $key;
      }
    }

    foreach ($this->header as $key => $label) {
      $this->len[$key] = mb_strlen($label);
    }

    if (is_array($align)) {
      $this->setAlign($align);
    }

    $this->addData($content);
  }

  /**
   * Overwrite the alignment array.
   *
   * @param array $align
   *   Alignment options [key => L|R|C, ...].
   */
  public function setAlign(array $align) {
    $this->align = $align;
  }

  /**
   * Add data to the table.
   *
   * @param array $content
   *   Content.
   */
  public function addData(array $content): TextTable {
    foreach ($content as &$row) {
      foreach ($this->header as $key => $value) {
        if (!isset($row[$key])) {
          $row[$key] = '-';
        }
        elseif (mb_strlen($row[$key]) > $this->maxlength) {
          $this->len[$key] = $this->maxlength;
          $row[$key] = substr($row[$key], 0, $this->maxlength - 3) . '...';
        }
        elseif (mb_strlen($row[$key]) > $this->len[$key]) {
          $this->len[$key] = mb_strlen($row[$key]);
        }
      }
    }

    $this->data = $this->data + $content;
    return $this;
  }

  /**
   * Add a delimiter.
   *
   * @return string
   *   Return $res
   */
  private function renderDelimiter(): string {
    $res = '|';
    foreach ($this->len as $key => $l) {
      $res .= (isset($this->align[$key]) && ($this->align[$key] == 'C' || $this->align[$key] == 'L') ? ':' : ' ')
        . str_repeat('-', $l)
        . (isset($this->align[$key]) && ($this->align[$key] == 'C' || $this->align[$key] == 'R') ? ':' : ' ')
        . '|';
    }
    return $res . "\r\n";
  }

  /**
   * Render a single row.
   *
   * @param array $row
   *   Passing $row renderRow().
   *
   * @return string
   *   return $res
   */
  private function renderRow(array $row): string {
    $res = '|';
    foreach ($this->len as $key => $l) {
      $res .= ' ' . $row[$key] . ($l > mb_strlen($row[$key]) ? str_repeat(' ', $l - mb_strlen($row[$key])) : '') . ' |';
    }

    return $res . "\r\n";
  }

  /**
   * Render the table.
   *
   * @param array $content
   *   Additional table content.
   *
   * @return string
   *   return $res
   */
  public function render(array $content = []): string {
    $this->addData($content);

    $res = $this->renderRow($this->header)
      . $this->renderDelimiter();
    foreach ($this->data as $row) {
      $res .= $this->renderRow($row);
    }

    return $res;
  }

}
