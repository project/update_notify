<?php

namespace Drupal\update_notify;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Notifier service.
 */
class NotifyService {
  use StringTranslationTrait;

  /**
   * Security icon.
   */
  const NOTICE_SECURITY = "🔒";

  /**
   * Unsupported icon.
   */
  const NOTICE_UNSUPPORTED = "⛔";

  /**
   * Major update icon.
   */
  const NOTICE_MAJOR = "⬆️";

  /**
   * Drupal\update_notify\UpdateInformationService definition.
   *
   * @var \Drupal\update_notify\UpdateInformationService
   */
  protected UpdateInformationService $updateInformation;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Drupal\Core\Config\ImmutableConfig definition.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Drupal\Core\Mail\MailManagerInterface definition.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $pluginManagerMail;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new NotifyService object.
   */
  public function __construct(UpdateInformationService $update_information, ConfigFactoryInterface $config_factory, DateFormatterInterface $date_formatter, StateInterface $state, LanguageManagerInterface $language_manager, MailManagerInterface $plugin_manager_mail, ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack) {
    $this->updateInformation = $update_information;
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('update_notify.settings');
    $this->dateFormatter = $date_formatter;
    $this->state = $state;
    $this->languageManager = $language_manager;
    $this->pluginManagerMail = $plugin_manager_mail;
    $this->moduleHandler = $module_handler;
    $this->loggerFactory = $logger_factory;
    $this->requestStack = $request_stack;
  }

  /**
   * Trigger notifications.
   *
   * @param bool $ignore_last_sent
   *   Optional parameter where you can ignore the last sent timestamp.
   *
   * @return array
   *   Status of the notification and count of updates found.
   */
  public function triggerNotifications(bool $ignore_last_sent = FALSE): array {
    $return = [
      'success' => FALSE,
      'count' => 0,
    ];

    if (!$this->config->get('enabled')) {
      return $return;
    }

    if ($this->readyToSend($ignore_last_sent)) {
      $updates = $this->updateInformation->checkForUpdates();

      if (empty($updates)) {
        return $return;
      }

      // Count the updates.
      $return['count'] = count($updates);

      // Try to send.
      $return['success'] = $this->sendNotifications($updates);

      // Update last & next send.
      if (!$ignore_last_sent) {
        $this->updateLastSent();
        $this->updateNextSend();
      }
      $this->loggerFactory->get('update_notify')->info($this->t('Update notifications have been triggered.'));
    }

    return $return;
  }

  /**
   * Returns the last sent time.
   *
   * @param bool $formatted
   *   Optional parameter if the date should be formatted or timestamp.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|mixed|string
   *   Return StringTranslation
   */
  public function getLastSent(bool $formatted = FALSE): mixed {
    $last_sent = $this->state->get('update_notify.last_sent');
    return $last_sent ? ($formatted ? $this->dateFormatter->format($last_sent) : $last_sent) : ($formatted ? $this->t('never') : 0);
  }

  /**
   * Sets the current time stamp.
   *
   * @param string $time
   *   Return $time.
   */
  public function updateLastSent(string $time = 'now'): void {
    $this->state->set('update_notify.last_sent', strtotime($time));
  }

  /**
   * Returns back the next send timestamp.
   *
   * @param bool $formatted
   *   Pass $formatted.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|mixed|string
   *   Return StringTranslation
   */
  public function getNextSend(bool $formatted = FALSE): mixed {
    $next_send = $this->state->get('update_notify.next_send');
    return $next_send ? ($formatted ? $this->dateFormatter->format($next_send) : $next_send) : ($formatted ? $this->t('unknown') : 0);
  }

  /**
   * U[date the next send time.
   */
  public function updateNextSend(): void {
    $time = $this->calculateNextSend();
    $this->state->set('update_notify.next_send', $time);
  }

  /**
   * Returns if we are ready to send a notification.
   *
   * @param bool $ignore_last_sent
   *   Return ignore_last_sent.
   *
   * @return bool
   *   Return bool
   */
  protected function readyToSend(bool $ignore_last_sent = FALSE): bool {
    if ($ignore_last_sent) {
      return TRUE;
    }

    return strtotime('now') >= $this->getNextSend();
  }

  /**
   * Calculates the next run time date.
   *
   * @return false|int
   *   Return false|int
   */
  protected function calculateNextSend(): false|int {
    $frequency = $this->config->get('frequency');
    switch ($frequency) {
      case 'weekly':
        return strtotime('next thursday 9:00am');

      case 'monthly':
        $now = strtotime('now');
        $this_month = strtotime('last thu of this month');
        $next_month = strtotime('last thu of next month');
        return $now > $this_month ? $this_month : $next_month;

      default:
        return strtotime('tomorrow 9:00am');
    }
  }

  /**
   * Trigger sending of all the notification types.
   *
   * @param array $updates
   *   Pass $update.
   */
  protected function sendNotifications(array $updates): bool {
    $status = TRUE;
    $message = $this->createMessage($updates, FALSE, -1);
    if ($this->sendEmail($message) === FALSE) {
      $status = FALSE;
      $this->loggerFactory->get('update_notify')->error($this->t('There was an error sending the email message'));
    }

    $message = $this->createMessage($updates, TRUE);
    if ($this->sendSlack($message) === FALSE) {
      $status = FALSE;
      $this->loggerFactory->get('update_notify')->error($this->t('There was an error sending the slack message'));
    }

    return $status;
  }

  /**
   * Builds the message for sending.
   *
   * @param array $updates
   *   Pass $update.
   * @param bool $mrkdwn
   *   Pass $mrkdwn.
   * @param int $text_limit
   *   Text limit to help with slack.
   *   Use a negative number to disable the limit.
   *
   * @return string
   *   Returns a message string.
   */
  protected function createMessage(array $updates, bool $mrkdwn = FALSE, int $text_limit = 2100): string {
    $include_key = $this->config->get('include_key');

    $include_host = $this->config->get('include_host');
    $custom_host = $this->config->get('custom_host');
    $host = $include_host ? ($custom_host ?: $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost()) : FALSE;

    $bold = ($mrkdwn) ? '*' : '';
    $wrapping = ($mrkdwn) ? '```' : '';
    $wrapping_single = ($mrkdwn) ? '`' : '';

    $message = [];
    $rows = [];

    // Table headers.
    $headers = [
      $this->t('Name'),
      $this->t('Current'),
      $this->t('Recommend'),
      $this->t('URL'),
      '',
    ];

    if ($host) {
      if ($mrkdwn) {
        $url = Url::fromRoute('update.status')->setAbsolute()->toString();
        $message[] = $this->t('Host:') . " <$url|$host>";
      }
      else {
        $message[] = $this->t('Host: @host', ['@host' => $host]);
      }
    }
    $message[] = $this->t('The following updates have been found.');
    $message[] = "";

    // Build the update table.
    foreach ($updates as $items) {
      foreach ($items as $update) {
        $notice = "ㅤ";
        if ($update['status']['security']) {
          $notice = self::NOTICE_SECURITY;
        }
        if ($update['status']['unsupported']) {
          $notice = self::NOTICE_UNSUPPORTED;
        }
        if ($update['recommended_release']['is_major']) {
          $notice .= self::NOTICE_MAJOR;
        }

        $rows[] = [
          $update['name'],
          $update['installed_version'],
          $update['recommended_release']['version'],
          $update['recommended_release']['url'],
          $notice,
        ];
      }
    }

    if ($rows) {
      $limit = $text_limit;
      $count = 0;
      $table_string = "";
      // Render the updates table.
      $table = new TextTable($headers, $rows);
      $table_render = $table->render();
      $table_array = preg_split("/\r\n|\n|\r/", $table_render);

      $output_tables = [];

      foreach ($table_array as $row) {
        $table_string .= $row . "\r\n";

        if ($count <= $limit || $limit < 0) {
          $output_tables[] = $table_string;
          $count = 0;
          $table_string = "";
        }

        $count += strlen($table_string);
      }

      foreach ($output_tables as $output) {
        if ($output != "") {
          $message[] = $wrapping . $output . $wrapping;
        }
      }
    }
    else {
      $message[] = $wrapping . $this->t('No updates have been found.') . $wrapping;
    }

    $extra_details = $this->getExtraDetails();
    if ($extra_details) {
      $table = new TextTable($extra_details['headers'], $extra_details['rows']);
      $message[] = $bold . $this->t("Extra details") . $bold;
      $message[] = $wrapping . $table->render() . $wrapping;
    }

    // If we want to show the key.
    if ($include_key) {
      $message[] = "";
      $message[] = "";
      $message[] = $bold . $this->t("Key:") . $bold;
      $message[] = $wrapping_single . $this->t("Security update: @icon", [
        '@icon' => self::NOTICE_SECURITY,
      ]) . $wrapping_single;
      $message[] = $wrapping_single . $this->t("Unsupported: @icon", [
        '@icon' => self::NOTICE_UNSUPPORTED,
      ]) . $wrapping_single;
      $message[] = $wrapping_single . $this->t("Major update: @icon", [
        '@icon' => self::NOTICE_MAJOR,
      ]) . $wrapping_single;
    }
    return implode("\n", $message);
  }

  /**
   * Returns some extra details.
   *
   * @return array[]|null
   *   Return array
   */
  protected function getExtraDetails(): ?array {
    $rows = [];
    $headers = [
      $this->t('Name'),
      $this->t('Version'),
    ];

    $include_php = $this->config->get('include_php');
    // $include_drush = $this->config->get('include_drush');
    // $include_node = $this->config->get('include_node');
    if ($include_php) {
      $rows[] = [$this->t('PHP Version'), phpversion()];
    }
    // Features coming soon.
    // if ($include_drush) {
    // $rows[] = [$this->t('Drush Version'), '10.0'];
    // }
    // if ($include_node) {
    // $rows[] = [$this->t('Node Version'), '16.0'];
    // }.
    return $rows ? ['rows' => $rows, 'headers' => $headers] : NULL;
  }

  /**
   * Send an email with the details.
   *
   * @param string $message
   *   Pass $message.
   *
   * @return bool|null
   *   Return bool|null
   */
  protected function sendEmail(string $message): ?bool {
    if (!$this->config->get('email')) {
      return NULL;
    }

    $email_to = $this->config->get('email_to');

    $module = 'update_notify';
    $key = 'update_notify';
    $to = $email_to ?? $this->configFactory->get('system.site')->get('mail');
    $params['message'] = $message;
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    $result = $this->pluginManagerMail->mail($module, $key, $to, $langcode, $params, NULL, TRUE);
    return (bool) $result['result'];
  }

  /**
   * Send a Slack notification with the details.
   *
   * @param string $message
   *   Pass $message.
   *
   * @return bool|null
   *   Pass bool|null
   */
  protected function sendSlack(string $message): ?bool {
    if (!$this->config->get('slack')) {
      return NULL;
    }

    if ($this->moduleHandler->moduleExists('slack')) {
      $slack_channel = $this->config->get('slack_channel');
      $channel = $slack_channel ?? $this->config->get('slack.settings')->get('slack_channel');
      $response = \Drupal::service('slack.slack_service')->sendMessage($message, $channel);
      return ($response && Response::HTTP_OK == $response->getStatusCode());
    }

    return FALSE;
  }

}
