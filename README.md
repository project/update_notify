# Update Notify

- [Introduction](#Introduction)
- [Requirements](#Requirements)
- [Installation](#Installation)
- [Configuration](#Configuration)

## Introduction

The aim of the update notify module is to try and give better notification 
details & communication methods when your site requires updates.

Simply getting an email saying you have some updates is not that useful as
it still requires you to go and find out all the changes.

With the Update Notify module:
- You can see higher level details of the updates
- Configure what you want to be notified about
- How often you want to be notified
- And choose various notification methods

## Requirements

## Installation

## Configuration
